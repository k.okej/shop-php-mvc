
		
		<section class="portfolio-block photography">
			<div class="row no-gutters">
				<div class="col-md-6 col-lg-4 item zoom-on-hover">
					<a><img class="img-fluid image" src="../../assets/img/nature/image5.jpg"></a>
				</div>
				<div class="col-md-6 col-lg-4 item zoom-on-hover">
					<a><img class="img-fluid image" src="../../assets/img/nature/image2.jpg"></a>
				</div>
				<div class="col-md-6 col-lg-4 item zoom-on-hover">
					<a><img class="img-fluid image" src="../../assets/img/tech/image4.jpg"></a>
				</div>
			</div><!-- end .row -->
        </section>
		
        <section class="portfolio-block block-intro">
            <div class="container">
                <div class="about-me">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam. Quisque semper justo at risus. Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui id orci.</p>
                    <h3>Like what you see?</h3>
					<a href="contact"><button class="btn btn-outline-primary btn-lg" type="button">Describe to me</button></a>
				</div>
			</div><!-- end .container -->
        </section>		
		
		<section class="portfolio-block projects-cards">
            <div class="container">
                <div class="heading">
                    <h2 id="work">Recent Work</h2>
                </div>
                <div class="row">
			<?php 
			$image = 0;
			foreach (Index::$result as $row) {
				if (isset($row['products_name'])) {
					?>
					<div class="col-md-6 col-lg-4">
                        <div class="project-card-no-image">
							
							<a><img src="../../assets/img/nature/image<?echo ++$image;?>.jpg?h=c5fb06440e9759bec9433393cd5a9761" alt="Card Image" class="card-img-top scale-on-hover"></a>
                            
							<div class="card-body" style="height: 150px">
								<h6><?echo $row['products_name'];?></h6>
								<p class="text-muted card-text"><?echo $row['products_description'];?></p>
                            </div>
							
							<a class="btn btn-outline-primary btn-sm" role="button">See More</a>
							
                            <div class="tags">
								<p>Price: <?echo $row['products_price'];?></p>
								<p class="text-muted"><?echo $row['category_name'];?></p>
							</div>
							
                        </div><!-- end .project-card-no-image -->
                    </div><!-- end .col-md-6 col-lg-4 -->
					<?php
				} 
				else {
					echo "We don't have a products yet.\n";
				}
			}
					?>

                </div><!-- end .row -->
            </div><!-- end .container -->
        </section>
		
		<section id="project" class="portfolio-block website gradient">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12 col-lg-5 offset-lg-1 text">
                    <h3>Website Project</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eget velit ultricies, feugiat est sed, efr nunc, vivamus vel accumsan dui. Quisque ac dolor cursus, volutpat nisl vel, porttitor eros.</p>
                </div>
                <div class="col-md-12 col-lg-5">
                    <div class="portfolio-laptop-mockup">
                        <div class="screen">
                            <div class="screen-content" style="background-image:url(&quot;assets/img/tech/image6.png?h=19ccc915f4703e4a2ebac4bf2082423d&quot;);"></div>
                        </div>
                        <div class="keyboard"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	
