		<section class="portfolio-block contact">
            <div class="container">
				
                <div class="heading">
                    <h2>Contact me</h2>
                </div>
				
				
                <form action="<? Contact::process() ?>" method="post" id="form-email">
					
                    <div class="form-group">
						<label for="subject">Subject</label>
						<input class="form-control item" type="text" name="subject" required="required" />
					</div>
					
                    <div class="form-group">
						<label for="email">Email</label>
						<input class="form-control item"
							   type="email"
							   name="email"
							   required="required"
							   value="<?php if (isset($_POST['email']))
								echo(htmlspecialchars($_POST['email']));?>"
						/>
					</div>
					
					<div class="form-group">
						<label for="email">Antispam - enter the current year</label>
						<input class="form-control item" type="text" name="year" required="required" />
					</div>
					
                    <div class="form-group">
						<label for="message">Message</label>
						<textarea class="form-control item" 
								  name="message"><?php 
							if (isset($_POST['message'])) 
								echo(htmlspecialchars($_POST['message']));
						?></textarea>
					</div>
					
                    <div class="form-group">
						<button class="btn btn-outline-primary btn-block btn-lg" type="submit" value="Send">Submit Form</button>
					</div>

					
                </form>
				
            </div><!-- end .container -->
        </section>