         <div class="collapse navbar-collapse" id="navbarNav">
                
			 <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item" role="presentation">
						<a class="nav-link" href="#">Home</a></li>
					<li class="nav-item" role="presentation">
						<a class="nav-link" href="#work">Recent Work</a></li>
					<li class="nav-item" role="presentation" style="border-right-style: dotted;">
						<a class="nav-link" href="#project">Website Project</a></li>
					
                    <li class="nav-item" role="presentation">
						<a class="nav-link" href="contact">Contact</a></li>
					<li class="nav-item" role="presentation">
						<a class="nav-link" href="http://infinitik.pl/cv">CV</a></li>
                    <li class="nav-item" role="presentation">
						<a class="nav-link" href="http://infinitik.pl">Infinitik</a></li>
                </ul>
			 
            </div>
        </div><!-- end .container -->
    </nav><!-- end nav -->
	
    <main class="page lanidng-page">