<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Home - Data Shop</title>
	
	<link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/bootstrap/css/shop.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,400,700">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
</head>

<body>
	
    <nav class="navbar navbar-expand-lg fixed-top gradient">
        <div class="container">
			<a class="navbar-brand logo" href="index">Data Shop</a>
			<button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav">
				<span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span>
			</button>
   