<?php

session_start();

function my_autoload($class_name) {
	
	if (file_exists('./controller/'.$class_name.'.controller.php')) {
		
        require_once './controller/'.$class_name.'.controller.php';
    }
	
	if (file_exists('./model/'.$class_name.'.class.php')) {
		
        require_once './model/'.$class_name.'.class.php';
			
	}
	
}

spl_autoload_register('my_autoload');

require_once('core/Controller.php');

require_once('core/routes.php');

?>