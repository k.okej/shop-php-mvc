<?php

Route::setRoute('', function() {
});

Route::setRoute('index.php', function() {
	Index::dbProducts();
	Index::CreateView('Header');
	Index::CreateView('GeneralNavigation');
    Index::CreateView('Index');
	Index::CreateView('Footer');
});

Route::setRoute('index', function() {
	Index::dbProducts();
	Index::CreateView('Header');
	Index::CreateView('GeneralNavigation');
    Index::CreateView('Index');
	Index::CreateView('Footer');
});

Route::setRoute('contact', function() {
	Contact::CreateView('Header');
	Contact::CreateView('SimpleNavigation');
    Contact::CreateView('Contact');
	Contact::CreateView('Footer');
});

Route::setRoute('user', function() {
	User::dbProducts();
	User::CreateView('Header');
	User::CreateView('SimpleNavigation');
    User::CreateView('User');
	User::CreateView('Footer');
});



?>