<?php

class Index extends Controller {
	
		public static $result = array();
	
        public static function dbProducts() {
       
			$DB = new Database(DBHost, DBPort, DBName, DBUser, DBPassword);
			self::$result=$DB->query("
				SELECT products_name, products_id, products_name, products_description, products_price, products_count, category_name
				FROM products
				INNER JOIN category 
				ON products.products_categoryid = category.category_id
									");
			//echo "Record count: {$DB->querycount}</br>";
			
			/* ----Elements in DB tab. products----
			 	products_id
				products_name
				products_description
				products_price
				products_tax
				products_count
				products_categoryid
			
			----Elements in DB tab. products----
				category_id
				category_name
			*/
			
			return self::$result;
        
    	}
	
}

?>